project = "dotnet-node"

app "dotnet-node" {
  build {
    use "docker" {
      disable_entrypoint = true
      dockerfile = "Containerfile"

      build_args = {
        BASE_IMAGE_VERSION = var.registry_image_tag
        BASE_IMAGE_TYPE    = var.registry_image_name
      }

      auth {
        username = var.dependency_registry_username
        password = var.dependency_registry_password
      }
    }

    registry {
      use "docker" {
        image = "${var.registry_image}/${var.commit_sha}/${var.registry_image_name}"
        tag   = var.registry_image_tag

        username = var.registry_username
        password = var.registry_password
      }
    }
  }

  deploy {
    use "nomad" {}
  }
}

variable "registry_username" {
  type        = string
  default     = "username"
  env         = ["CI_REGISTRY_USER"]
  description = "The username to log in to the docker registry"
  sensitive   = false
}

variable "registry_password" {
  type        = string
  default     = "username"
  env         = ["CI_REGISTRY_PASSWORD"]
  description = "The password to log in to the docker registry"
  sensitive   = true
}

variable "registry_image" {
  type        = string
  env         = ["CI_REGISTRY_IMAGE"]
  description = "The docker image name"
  sensitive   = false
}


variable "registry_image_name" {
  type        = string
  env         = ["IMAGE_NAME"]
  description = "The docker image name"
  sensitive   = false
}

variable "commit_sha" {
  type        = string
  env         = ["CI_COMMIT_SHORT_SHA"]
  description = "The docker image tag"
  sensitive   = false
}

variable "registry_image_tag" {
  type        = string
  env         = ["IMAGE_TAG"]
  description = "The docker image tag"
  sensitive   = false
}

variable "dependency_registry_username" {
  type        = string
  description = "The username to log in to the docker registry"
  env         = ["CI_DEPENDENCY_PROXY_USER"]
  sensitive   = false
}

variable "dependency_registry_password" {
  type        = string
  description = "The password to log in to the docker registry"
  env         = ["CI_DEPENDENCY_PROXY_PASSWORD"]
  sensitive   = true
}
